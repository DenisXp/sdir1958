package repository.test;

import org.junit.Before;
import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.assertTrue;

/**
 * Created by Xperrty on 4/15/2018.
 */
public class EmployeeImplTest {
    private EmployeeImpl repo;
    @Before
    public void setUp() {
        repo = new EmployeeImpl();
    }

    @Test
    public void testValidLastName() {
        Employee e1 = new Employee("test","12345678911",DidacticFunction.ASISTENT,"1");
        repo.addEmployee(e1);
        assertTrue( repo.getEmployeeList().size() ==1 );
    }
}
