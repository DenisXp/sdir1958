package controller.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Xperrty on 5/1/2018.
 */
public class IncrementIntegrationTest {
    private EmployeeController controller;
    private EmployeeImpl repo;
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller2;
    private EmployeeValidator employeeValidator;
    @Before
    public void setUp() {
        PrintWriter writer ;
        try {
            writer = new PrintWriter("employeeDB/test.txt");
            writer.print("");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        repo = new EmployeeImpl("employeeDB/test.txt");
        controller = new EmployeeController(repo);
        Employee e1 = new Employee("testA","1234567890123", DidacticFunction.ASISTENT,"12");
        Employee e2= new Employee("testB","1234557890123", DidacticFunction.ASISTENT,"22");
        controller.addEmployee(e1);
        controller.addEmployee(e2);

        employeeRepository = new EmployeeMock();
        controller2         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();


    }

    @Test
    public void testA() {
        assertFalse(controller2.getEmployeesList().isEmpty());
        assertEquals(6, controller2.getEmployeesList().size());
        Employee newEmployee = new Employee("ValidLastName", "1910509055057", DidacticFunction.ASISTENT, "3000");
        assertTrue(employeeValidator.isValid(newEmployee));
        controller2.addEmployee(newEmployee);
        assertEquals(7, controller2.getEmployeesList().size());
        assertTrue(newEmployee.equals(controller2.getEmployeesList().get(controller2.getEmployeesList().size() - 1)));
    }

    @Test
    public void testAB(){
        Employee e1 = new Employee("newEmployee","1111111111111", DidacticFunction.ASISTENT,"1234");
        controller.addEmployee(e1);
        assertEquals(controller.getEmployeesList().size(),3);
        assertTrue(controller.changeFunction("newEmployee","LECTURER"));
    }

    @Test
    public void testABC(){
        Employee e1 = new Employee("newEmployee","1111111111111", DidacticFunction.ASISTENT,"1234");
        controller.addEmployee(e1);
        assertEquals(controller.getEmployeesList().size(),3);
        assertTrue(controller.changeFunction("newEmployee","LECTURER"));
        assertTrue(controller.sortSalary().get(0).getSalary().equals("1234"));
        assertTrue(controller.sortAge().get(0).getLastName().equals("newEmployee"));
    }



}
