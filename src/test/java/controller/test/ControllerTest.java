package controller.test;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.validator.EmployeeValidator;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Xperrty on 5/1/2018.
 */
public class ControllerTest {
    private EmployeeController controller;
    private EmployeeImpl repo;
    @Before
    public void setUp() {
        PrintWriter writer ;
        try {
            writer = new PrintWriter("employeeDB/test.txt");
            writer.print("");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        repo = new EmployeeImpl("employeeDB/test.txt");
        controller = new EmployeeController(repo);
        Employee e1 = new Employee("testA","1234567890123", DidacticFunction.ASISTENT,"12");
        Employee e2= new Employee("testB","1234557890123", DidacticFunction.ASISTENT,"22");
        controller.addEmployee(e1);
        controller.addEmployee(e2);


    }

    @Test
    public void testInvalidName() {

        assertFalse(controller.changeFunction("a",DidacticFunction.ASISTENT.toString()));
    }
    @Test
    public void testInvalidFunction() {
        assertFalse(controller.changeFunction("test","Nimeni"));
    }
    @Test
    public void goodChange() {

        assertTrue(controller.changeFunction("testA",DidacticFunction.TEACHER.toString()));
    }

    @Test
    public void sortSalary() {
        assertTrue(controller.sortSalary().get(0).getSalary().equals("22"));
    }

    @Test
    public void sortCnp() {
        assertTrue(controller.sortAge().get(0).getLastName().equals("testA"));

    }
}
