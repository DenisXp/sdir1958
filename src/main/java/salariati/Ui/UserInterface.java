package salariati.Ui;

import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by Xperrty on 3/28/2018.
 */
public class UserInterface {
    private EmployeeController controller;

    public UserInterface(EmployeeController controller){
        this.controller = controller;
    }

    public void start(){
        while (true) {
            System.out.println("Meniu:");
            System.out.println("1.Add Teacher:");
            System.out.println("2.Change:");
            System.out.println("3.Sort age:");
            System.out.println("4.Sort Salary:");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String s = "";
            try {
                s = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (s == "1") {
                addUser();
            } else if (s == "2") {
                changeProfesion();
            } else if (s == "3") {
                sortAge();
            } else if (s == "4") {
                sortSalary();
            } else if (s == "0") break;
            else {
                System.out.println("Invalid command!");
            }
        }
    }


        private void addUser(){
            String name = getStr("Name:");
            String cnp = getStr("CNP:");
            String function = getStr("Didactic Function:");
            String salary = getStr("Salary");
            DidacticFunction finalFunction = DidacticFunction.valueOf(function.toUpperCase());
            controller.addEmployee(new Employee(name,cnp,finalFunction,salary));

        }

        private void changeProfesion(){
            String name = getStr("Name:");
            Employee last = controller.getByName(name);
            if (last == null){
                System.out.println("Name not found");
                return;
            }
            String function = getStr("New Didactic Function:");
            DidacticFunction finalFunction = DidacticFunction.valueOf(function.toUpperCase());
            Employee newEmployee = new Employee(last.getLastName(),last.getCnp(),finalFunction,last .getSalary());
            controller.modifyEmployee(last,newEmployee);
        }


        private void sortAge(){
        printList(controller.sortAge());

        }

        private void sortSalary(){
            printList(controller.sortSalary());

        }


        private void printList(List<Employee>list){
            for (Employee e :list) {
                System.out.println(e.toString());
            }
        }


        private String getStr(String statement){
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String s = "";
            System.out.println(statement);
            try {
                s = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return s;
        }

}
