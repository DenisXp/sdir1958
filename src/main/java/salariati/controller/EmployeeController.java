package salariati.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;

import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;

import javax.rmi.CORBA.Util;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;
	
	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
	}
	
	public void addEmployee(Employee employee) {
		employeeRepository.addEmployee(employee);
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}
	
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		employeeRepository.modifyEmployee(oldEmployee, newEmployee);
	}
	public Employee getByName(String name){
		List<Employee> list= getEmployeesList();
		for (Employee e :list) {
			if (e.getLastName().equals(name)) return  e;
		}
		return null;
	}


	public List<Employee> sortAge(){
		List<Employee> list = getEmployeesList();
		Collections.sort(list, new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				if (o1.getAge()< o2.getAge())return -1;
				else if (o1.getAge()>o2.getAge())return 1;
				return 0;
			}
		});
		return list;
	}

	public List<Employee> sortSalary(){
		List<Employee> list = getEmployeesList();
		Collections.sort(list, new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				if (Float.parseFloat(o1.getSalary())< Float.parseFloat(o2.getSalary()))return 1;
				else if (Float.parseFloat(o1.getSalary())>Float.parseFloat(o2.getSalary()))return -1;
				return 0;
			}
		});
		return list;
	}

	public boolean changeFunction(String name, String newFunction){
		Employee e = getByName(name);
		if ( e == null ) return false;
		DidacticFunction f ;
		try {
			f= DidacticFunction.valueOf(newFunction);
		}
		catch (Exception ex){
			return false;
		}
		e.setFunction(f);
		return true;
	}

	public void deleteEmployee(Employee employee) {
		employeeRepository.deleteEmployee(employee);
	}
	
}
